﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {
    [Class(Description =
        "Used from -" + nameof(EmployeeP.Photo) + "- and -" + nameof(CategoryP.Picture) + "-.\r\n" + 
        "\r\n" +
        "See -" + nameof(PKHTMLAttribute) + "-.\r\n" 
    )]
    public class PictureEncoder : IHTMLEncoder {

        [ClassMember(Description =
            "Encodes the given Base64 encoding of an image directly into an HTML <IMG> tag.\r\n" +
            "\r\n" +
            "Note that this normally not an efficient solution. The browser has no means of caching the picture now.\r\n" +
            "\r\n" +
            "Note how -" + nameof(Converter.ConvertDatabase) + "- has already converted the original data from Hex to Base64 and also removed the OLE header of 78 bytes, " +
            "so we just display the raw string here.\r\n" +
            "\r\n"
        )]
        public static string Encode(string base64String) {
            try {
                // Avoid security implications by asserting that string is actually a valid Base64 string.
                // (so one can not insert HTML / Javascript in the picture data-definition)
                // If not a valid Base64 string we except to get something similar to:
                // Exception: FormatException 
                // with Message: The input is not a valid Base-64 string as it contains a non-base 64 character, more than two padding characters, or an illegal character among the padding characters.            
                var assert = Convert.FromBase64String(base64String);
            } catch (Exception ex) {
                // Note how we fail gracefully. 
                return "<p>" + System.Net.WebUtility.HtmlEncode("Invalid Base64 encoding for '" + base64String.Substring(0, Math.Min(base64String.Length, 30)) + "...'. Unable to create picture. Exception " + ex.GetType() + " with message '" + ex.Message) + "'</p>";
            };
            return "<img src=\"data:image/bmp;base64," + base64String + "\"/>";
        }
    }
}