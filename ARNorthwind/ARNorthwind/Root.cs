﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARNorthwind {

    [Class(Description =
        "Contains the user friendly welcome text (front page) for the application.\r\n" +
        "\r\n" +
        "An instance of this class is used as root-element in -" + nameof(ARCAPI.DataStorage.Storage) + "-.\r\n" +
        "Used in order for -" + nameof(ToHTMLSimpleSingle) + "- to override the default rather useless listing of API internal information."
    )]
    public class Root : PRich {

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Contains hand coded HTML introducing ARNorthwind.\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null) =>
            "<h1>ARNorthwind</h1>\r\n" +
            "<p>ARNorthwind is a demonstration of <a href=\"https://bitbucket.org/BjornErlingFloetten/ARCore\">AgoRapide 2020 (ARCore)</a> " +
            "with data from Microsoft's Northwind example dataset.</p>\r\n" +
            "<p>An online example of this application is found at <a href=\"http://ARNorthwind.AgoRapide.com\">http://ARNorthwind.AgoRapide.com</a></p>\r\n" +
             "\r\n" +
            (
                /// Information from <see cref="PropertyStreamLine.StoreFailure"/>
                /// TODO: Link this better together, create separate class called "ParseOrStoreFailure"
                !Program.DataStorage.Storage.TryGetP<IP>("ParseOrStoreFailure", out var f) ? "" : (
                    "<p style=\"color:red\">ERROR: " +
                    "Some -" + nameof(ARConcepts.PropertyStream) + "- content did not parse correctly. " +
                    "Details: <a href=\"ParseOrStoreFailure\">ParseOrStoreFailure</a></p>\r\n"
            )) +
            "\r\n" +
           "<h2>Some sample queries</h2>\r\n" +
            "<p>" +
            "Note how queries, although predefined here, do not have to be. " +
            "You can execute any query directly on-the-fly in your browser as long as you can construct the correct URL for it." +
            "</p>\r\n" +
            "<p>For more sample queries, see also <a href=\"http://ARAdventureWorksOLAP.AgoRapide.com\">http://ARAdventureWorksOLAP.AgoRapide.com</a></p>\r\n" +
            "<table><tr><th>Explanation</th><th>URL</th></tr>\r\n" +
            string.Join("", new List<(string url, string text)> {
                /// TODO: <see cref="ARConcepts.LinkInsertionInDocumentation"/> currently not possible here because would get link in link-text
                /// TODO: Use both Title and Description, and have links in Description instead
                ("SELECT CountP AS NumberOfEntities, CountPRec AS NumberOfPropertiesOfEntities, ToPropertyStream.Length() AS SizeInDatabase/ORDER BY NumberOfEntities DESC", "Database statistics"),
                ("Employee/SELECT Name, City, SupervisorId, Supervisor.Name, Supervisor.City, Subordinate.Name.Count() AS DirectSubordinates/ORDER BY Name", "Employees with Supervisors and Subordinates"),
                ("Order/AGGREGATE Employee.Name/ORDER BY _SUM DESC", "Count orders by employee"),
                ("Order/AGGREGATE Employee.Supervisor.Name/ORDER BY _SUM DESC", "Count orders by supervisor"),
                ("Employee/SELECT Name, Order.OrderId.Count() AS OrderCount/ORDER BY OrderCount DESC", "Count orders by employee (through " + nameof(ForeignKey) + " or " + nameof(FunctionKeyAggregateSum) + ")"),
                ("Employee/SELECT Name, Order.OrderDetail.Sum.Sum() AS OrderSum/ORDER BY OrderSum DESC/TAKE 10", "Order sums by employee, top-ten"),
                ("Order/WHERE OrderDate = Last12Weeks/PIVOT Employee.Name BY OrderDate.YearWeek()/ORDER BY _SUM DESC", "Order count by employee, last 12 weeks"),
                ("Order/AGGREGATE OrderDate.Year()/ORDER BY OrderDate.Year()Id DESC", "Count orders by year"),
                ("Order/PIVOT Employee.Name BY OrderDate.Year()/ORDER BY " + DateTime.UtcNow.Year + " DESC", "Count orders by year employee and year"),
                ("Order/WHERE ShippedDate = Today/SELECT OrderId, ShippedDate, CustomerId, Customer.CompanyName, OrderDetail.Sum.Sum() AS SumOrder", "Orders shipped today"),
                ("Order/WHERE ShippedDate = Yesterday/SELECT OrderId, ShippedDate, CustomerId, Customer.CompanyName, OrderDetail.Sum.Sum() AS SumOrder", "Orders shipped yesterday"),
                ("Order/WHERE ShippedDate EQ NULL/WHERE RequiredDate = NextWeekG/SELECT OrderId, RequiredDate, CustomerId, Customer.CompanyName, OrderDetail.Sum.Sum() AS SumOrder/ORDER BY RequiredDate", "Orders that must ship within the next 7 days"),
                ("Order/WHERE ShippedDate EQ NULL/SELECT OrderId, RequiredDate, CustomerId, Customer.CompanyName, OrderDetail.Sum.Sum() AS SumOrder/ORDER BY RequiredDate", "Order reserve"),
                ("Order/WHERE ShippedDate EQ NULL/WHERE RequiredDate = Next6MonthsG/SELECT OrderId, RequiredDate, CustomerId, Customer.CompanyName, OrderDetail.Sum.Sum() AS SumOrder/ORDER BY RequiredDate", "Order reserve, next 6 months"),
                ("Order/WHERE ShippedDate EQ NULL/AGGREGATE RequiredDate.YearMonth() SUM OrderDetail.Sum.Sum()/ORDER BY RequiredDate.YearMonth()Id", "Order reserve, per month"),
                ("OrderDetail/PIVOT Product.Name BY Order.OrderDate.Year()/ORDER BY " + DateTime.UtcNow.Year + " DESC", "Count of orderlines with given product, per year"),
                ("OrderDetail/PIVOT Product.Name BY Order.OrderDate.Year() SUM Quantity/ORDER BY " + DateTime.UtcNow.Year + " DESC", "Sum of product quantity, per year"),
                ("OrderDetail/PIVOT Product.Name BY Order.OrderDate.Year() SUM Sum/ORDER BY " + DateTime.UtcNow.Year + " DESC/TAKE 10", "Order sum of product, per year, top-ten this year"),
                ("OrderDetail/SELECT Order.OrderDate, Order.Customer.CompanyName, Product.Category.Name, Product.Name, *, Sum/ORDER BY Order.OrderDate DESC/SKIP 0/TAKE 100", "Last 100 order details (order lines) (what are we currently selling)"),
                ("Customer/SELECT CustomerId, CompanyName, Order.OrderDetail.Sum.Sum() AS OrderSum/ORDER BY OrderSum DESC/TAKE 10", "Most valuable customers (top ten)"),
                ("Customer/SELECT CustomerId, CompanyName, Order.OrderDetail.Sum.Avg() AS OrderAvg/ORDER BY OrderAvg DESC", "Average order value per customer"),
                ("Product/SELECT Name, OrderDetail.Sum.Sum() AS OrderSum/ORDER BY OrderSum DESC/TAKE 10", "Most sold products by value (top ten)"),
                ("Product/SELECT Name, OrderDetail.Quantity.Sum().Int() AS OrderQuantity/ORDER BY OrderQuantity DESC/TAKE 10", "Most sold products by quantity / count (top ten)"),
                ("Category/SELECT Name, Description, Product.OrderDetail.Sum.Sum() AS OrderSum/ORDER BY OrderSum DESC","Order sum per category"),
                ("Order/WHERE ShippedDate NEQ NULL/PIVOT ShippedDate.Year() BY Shipper.CompanyName/ORDER BY ShippedDate.Year()Id DESC","Count of Orders per Shipper per year"),
                ("Employee/SELECT Name, EmployeeTerritory.Territory.Region.Description.Distinct().Join() AS Regions, EmployeeTerritory.Territory.Region.RegionId.Distinct().Count() AS Count","Assigned regions per Employee (note: Always 1)"),
                ("Region/SELECT Description, Territory.EmployeeTerritory.Employee.Order.OrderDetail.Sum.Sum() AS OrderSum/ORDER BY Description","Sum Orders per Region (only meaningful if no Employee assigned to different Regions)"),
                ("Territory/SELECT Region.Description, Description, EmployeeTerritory.Employee.Name.Join() AS Names, EmployeeTerritory.Employee.EmployeeId.Join() AS EmployeeId, EmployeeTerritory.Employee.EmployeeId.Count() AS Count/ORDER BY Names DESC","Assigned Employees per Territory (Note: Maxium is 1)"),
                ("Employee/SELECT EmployeeId, Name, EmployeeTerritory.Territory.Description.Join() AS Territories, EmployeeTerritory.Employee.Order.OrderDetail.Sum.Sum() AS OrderSum/ORDER BY Name","Assigned Territories per Employee with Employee's sum of Orders")
            }.Select(tuple =>
                "<tr><td>" +
                "<a href=\"" + nameof(PSPrefix.dt) + "/" + tuple.url + "/TITLE " + tuple.text + "\">" +
                System.Net.WebUtility.HtmlEncode(tuple.text) +
                "</a>" +
                "</td><td>" +
                System.Net.WebUtility.HtmlEncode(tuple.url).Replace("/", "<br>") +
                "</td></tr>"
            )) +
            "</table>\r\n" +
            "\r\n" +
            "<h2>Database entities</h2>\r\n" +
            "<table><tr><th>Entity</th><th>Count</th></tr>" +
            string.Join("", Program.DataStorage.Storage[nameof(PSPrefix.dt)].
                OrderBy(ikip => ikip.Key.ToString()).
                Select(ikip =>
                    "<tr><td>" +
                    "<a href=\"" + nameof(PSPrefix.dt) + "/" + System.Net.WebUtility.UrlEncode(ikip.Key.ToString()) + "/All\">" +
                    System.Net.WebUtility.HtmlEncode(ikip.Key.ToString()) +
                    "</a>" +
                    "</td><td align=right>" +
                    Program.DataStorage.Storage[nameof(PSPrefix.dt)].GetP<IP>(ikip.Key).Count() +
                    "</td></tr>"
                )
             ) +
            "</table>\r\n" +
            "\r\n" +
            "<h2>Miscellaneous links</h2>\r\n" +
            string.Join("", new List<(string url, string text)> {
                (nameof(PSPrefix.doc),"AgoRapide (ARCore) documentation"),
                (nameof(PSPrefix.app),"Internal application state")
            }.Select(tuple =>
                "<p><a href=\"" + System.Net.WebUtility.UrlEncode(tuple.url) + "\">" +
                System.Net.WebUtility.HtmlEncode(tuple.text) + "</a></p>"
            )) +
            "\r\n" +
            "<h2>Other information</h2>\r\n" +
            "<p>Author: Bjørn Erling Fløtten, Trondheim, Norway. bef_northwind@bef.no</p>\r\n" +
            "<p>The date and time values in the example database are modified at application startup in order to look fresh, " +
            "and in order for queries like <a href=\"dt/Order/WHERE OrderDate = Today\">today's orders</a> to function.</p>\r\n" +
            "<p>" +
            "Database original SQL dump was of size 2 MB.<br>\r\n" +
            "The AgoRapide -" + nameof(ARConcepts.PropertyStream) + "- conversion of the same data is only 1.3 MB.<br>\r\n" +
            "In-memory footprint of this application (without any optimizations) is around 12 MB (Baseline memory snapshot done in Visual Studio's Performance Profiler)." +
            "</p>\r\n" +
            "<p>The Northwind database can be found at<br>\r\n" +
            "https://github.com/microsoft/sql-server-samples/blob/master/samples/databases/northwind-pubs/instnwnd.sql <br>\r\n" +
            "with the following license:<br>\r\n" +
            "https://github.com/microsoft/sql-server-samples/blob/master/license.txt <br>\r\n" +
            "</p>\r\n";
    }
}