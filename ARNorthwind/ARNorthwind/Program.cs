// Copyright (c) 2016-2020 Bj�rn Erling Fl�tten, Trondheim, Norway

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using ARCCore;
using ARCDoc;
using ARCAPI;

namespace ARNorthwind {

    [Class(Description = "Application startup and initialization")]
    public class Program {

        public static readonly IK NodeId = IKString.FromString(System.Environment.MachineName);
        public static DataStorage DataStorage { get; private set; } = null!; // Initialized here only in order to get rid of compilation warning. 

        public static void Main(string[] args) {
            System.Threading.Thread.CurrentThread.Name = "MainThread";
            /// Include all assemblies in which your controllers and <see cref="AgoRapide.BaseEntity"/>-derived classes resides.
            UtilCore.Assemblies = new List<System.Reflection.Assembly> {
                typeof(ARCCore.ClassAttribute).Assembly,
                typeof(ARCDoc.Documentator).Assembly,
                typeof(ARCQuery.QueryExpression).Assembly,
                typeof(ARCAPI.BaseController).Assembly,
                typeof(ARNorthwind.Program).Assembly
                };

            using var streamProcessor = StreamProcessor.CreateBareBonesInstance(NodeId);
            DataStorage = DataStorage.Create(NodeId, storage: new Root(), streamProcessor);
            InitStreamProcessor(streamProcessor);
            Converter.ConvertDatabase(streamProcessor);
            AddController.AllowFilter = new List<Subscription> {
                Subscription.Parse("-*") // June 2021: No edits allowed. Experience showed that damaging edits were done which messed up the basic structure.
            };
            Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>()).Build().Run();
        }

        [ClassMember(Description =
            "Initializes stream processor.\r\n" +
            "Must be called after -" + nameof(DataStorage) + "- has been initialized."
        )]
        public static void InitStreamProcessor(StreamProcessor streamProcessor) {

            var daysToAdd = (int)DateTime.UtcNow.Subtract(new DateTime(1998, 5, 6)).TotalDays; // Last Order.ShippedDate in database

            // TODO: Consider using subscription here instead.
            // /// <see cref="Subscription"/> is used here for pedagogical reasons. It is more efficient to compare direct against 'dt/'.
            // var subscription = Subscription.Parse("+" + nameof(PropertyStreamLinePrefix.dt) + "/");
            var dt = nameof(PSPrefix.dt) + "/";

            streamProcessor.OutsideLocalReceiver = s => {
                // Initial version of OutsideLocalReceiver, only used at application startup
                // Note absence of locking here (because single threaded) (we could as well have used locking, it really makes no difference as there will be no contention anyway)

                if (!s.StartsWith(dt)) return;

                // Note more strict handling at application startup. If fails then application will not start.
                var pos = s.IndexOf("Date = ");
                if (pos > 1) {
                    pos += "Date = ".Length;
                    if (pos + 10 <= s.Length && UtilCore.DateTimeTryParse(s.Substring(pos, 10), out var dateTime) && dateTime.Year < 2021) {
                        /// This looks like an old date
                        /// Bring dates "up-to-date", in order for the database to look fresh
                        /// Note that any new date values added after conversion may look strange now, as they will not be changed.
                        /// (we do not know WHEN they where changed with the current setup (not using <see cref="PP.Created"/>
                        dateTime = dateTime.AddDays(daysToAdd);
                        s = s.Substring(0, pos) + dateTime.ToString("yyyy-MM-dd") + s[(pos + 10)..];
                    }
                }
                PropertyStreamLine.ParseAndStore(DataStorage.Storage, s);
            };

            // This call may take some time.
            streamProcessor.Initialize();

            streamProcessor.OutsideLocalReceiver = s => {
                // 'Permanent' (within application lifetime) version of OutsideLocalReceiver
                if (!s.StartsWith(dt)) return;
                try {
                    DataStorage.Lock.EnterWriteLock(); // Use locking because this is a multi threaded context now.

                    // Note less strict handling when application is running.
                    /// The last failure can at any time be found at root-level in the data storage 
                    /// with the key 'ParseOrStoreFailure' (see <see cref="PropertyStreamLine.StoreFailure"/>).
                    PropertyStreamLine.ParseAndStoreFailSafe(DataStorage.Storage, s);
                } finally {
                    DataStorage.Lock.ExitWriteLock();
                }
            };
        }
    }
}