// Copyright (c) 2016-2020 Bj�rn Erling Fl�tten, Trondheim, Norway
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ARNorthwind {
    public class Startup {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) => services.AddControllers();

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();

            app.UseRouting();

            //// Added 10 May 2021 in order to deploy on Linux using proxy server. 
            // Removed 11 May 2021, not necessary after all
            //app.UseForwardedHeaders(new ForwardedHeadersOptions {
            //    ForwardedHeaders = 
            //        Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor | 
            //        Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto
            //});

            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}