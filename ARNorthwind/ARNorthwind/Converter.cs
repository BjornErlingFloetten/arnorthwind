﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description =
        "Converts original sample database to property stream format.\r\n" +
        "\r\n" +
        "See -" + nameof(ConvertDatabase) + "-."
    )]
    public static class Converter {
        [ClassMember(Description =
            "Converts original sample database to property stream format if no data found.\r\n" +
            "\r\n" +
            "The parsing is quick-and-dirty and based on source file as it appeared on 11 Feb 2021 (it had then not been changed since 2 Nov 2018).\r\n" +
            "\r\n" +
            "This code will normally not have to be run, since its output ( a -" + nameof(ARConcepts.PropertyStream) + "- representation of the sample database) " +
            "is contained in the git-repository of ARNorthwind anyway"
        )]
        public static void ConvertDatabase(StreamProcessor streamProcessor) {
            var key = nameof(Category);
            if (!Program.DataStorage.Storage[nameof(PSPrefix.dt)].ContainsKey(key)) {
                var logger = (IP)new PConcurrent();
                logger.Logger = s => streamProcessor.SendFromLocalOrigin(PSPrefix.app + "/" + Program.NodeId + "/" + nameof(Converter) + "/" + s);
                logger.LogKeyValue("Key", key);
                logger.LogText("Key not found in storage. Assuming that must perform conversion.");
                var sourcePath = Environment.CurrentDirectory + System.IO.Path.DirectorySeparatorChar + "instnwnd.sql";
                logger.LogKeyValue(nameof(sourcePath), sourcePath);
                if (!System.IO.File.Exists(sourcePath)) {
                    logger.LogText(nameof(sourcePath) + " not found. Throwing exception.");
                    throw new NorthwindConversionException(
                        nameof(sourcePath) + "\r\n" + sourcePath + "\r\nnot found.\r\n" +
                        "\r\n" +
                        "Resolution: Download from\r\n" +
                        "https://raw.githubusercontent.com/microsoft/sql-server-samples/master/samples/databases/northwind-pubs/instnwnd.sql\r\n" +
                        "and restart application.\r\n"
                    );
                }
                logger.LogText("ReadAllLines");

                var strSource = System.IO.File.ReadAllText(sourcePath);
                logger.LogText("Removing superfluous line breaks");
                new List<(string oldValue, string newValue)> {
                    // Spurious line breaks
                    ("'Coventry House\r\n", "'Coventry House"),
                    ("'Edgeham Hollow\r\n", "'Edgeham Hollow"),
                // Line breaks related to INSERT INTO "Orders"
                    ("INSERT INTO \"Orders\"\r\n", "INSERT INTO \"Orders\" "),
                    (",\r\n\t", ","),
                    ("\")\r\nVALUES (", "\") VALUES (")
                }.ForEach(tuple => {
                    logger.LogKeyValue(nameof(tuple.oldValue), tuple.oldValue);
                    logger.LogKeyValue(nameof(tuple.newValue), tuple.newValue);
                    strSource = strSource.Replace(tuple.oldValue, tuple.newValue);
                });

                var source = strSource.Split("\r\n");
                logger.LogKeyValue(nameof(source) + ".Length", source.Length.ToString());

                // Data size is pretty small, in-memory processing and few I/O operation is the most efficient way

                var ps = new List<string>();

                var filters = new List<(string start, string entityType, List<PK> fields)> {
                    (
                        "INSERT \"Categories\"(\"CategoryID\",\"CategoryName\",\"Description\",\"Picture\") VALUES(",
                        nameof(Category),
                        new List<PK> {
                            PK.FromEnum(CategoryP.Name),
                            PK.FromEnum(CategoryP.Description),
                            PK.FromEnum(CategoryP.Picture),
                        }
                    ),
                    (
                        "INSERT \"Customers\" VALUES(",
                        nameof(Customer),
                        new List<PK> {
                            PK.FromEnum(CustomerP.CompanyName),
                            PK.FromEnum(CustomerP.ContactName),
                            PK.FromEnum(CustomerP.ContactTitle),
                            PK.FromEnum(CustomerP.Address),
                            PK.FromEnum(CustomerP.City),
                            PK.FromEnum(CustomerP.Region),
                            PK.FromEnum(CustomerP.PostalCode),
                            PK.FromEnum(CustomerP.Country),
                            PK.FromEnum(CustomerP.Phone),
                            PK.FromEnum(CustomerP.Fax)
                        }
                    ),
                    (
                        "INSERT \"Employees\"(\"EmployeeID\",\"LastName\",\"FirstName\",\"Title\",\"TitleOfCourtesy\",\"BirthDate\",\"HireDate\",\"Address\",\"City\",\"Region\",\"PostalCode\",\"Country\",\"HomePhone\",\"Extension\",\"Photo\",\"Notes\",\"ReportsTo\",\"PhotoPath\") VALUES(",
                        nameof(Employee),
                        new List<PK> {
                            PK.FromEnum(EmployeeP.LastName),
                            PK.FromEnum(EmployeeP.FirstName),
                            PK.FromEnum(EmployeeP.Title),
                            PK.FromEnum(EmployeeP.TitleOfCourtesy),
                            PK.FromEnum(EmployeeP.BirthDate),
                            PK.FromEnum(EmployeeP.HireDate),
                            PK.FromEnum(EmployeeP.Address),
                            PK.FromEnum(EmployeeP.City),
                            PK.FromEnum(EmployeeP.Region),
                            PK.FromEnum(EmployeeP.PostalCode),
                            PK.FromEnum(EmployeeP.Country),
                            PK.FromEnum(EmployeeP.HomePhone),
                            PK.FromEnum(EmployeeP.Extension),
                            PK.FromEnum(EmployeeP.Photo),
                            PK.FromEnum(EmployeeP.Notes),
                            PK.FromEnum(EmployeeP.SupervisorId),
                            PK.FromEnum(EmployeeP.PhotoPath),
                        }
                    ),
                    (
                        "INSERT \"Order Details\" VALUES(",
                        nameof(OrderDetail),
                        new List<PK> {
                            PK.FromEnum(OrderDetailP.OrderId),
                            PK.FromEnum(OrderDetailP.ProductId),
                            PK.FromEnum(OrderDetailP.UnitPrice),
                            PK.FromEnum(OrderDetailP.Quantity),
                            PK.FromEnum(OrderDetailP.Discount),
                        }
                    ),
                    (
                        "INSERT \"Products\"(\"ProductID\",\"ProductName\",\"SupplierID\",\"CategoryID\",\"QuantityPerUnit\",\"UnitPrice\",\"UnitsInStock\",\"UnitsOnOrder\",\"ReorderLevel\",\"Discontinued\") VALUES(",
                        nameof(Product),
                        new List<PK> {
                            PK.FromEnum(ProductP.Name),
                            PK.FromEnum(ProductP.SupplierId),
                            PK.FromEnum(ProductP.CategoryId),
                            PK.FromEnum(ProductP.QuantityPerUnit),
                            PK.FromEnum(ProductP.UnitPrice),
                            PK.FromEnum(ProductP.UnitsInStock),
                            PK.FromEnum(ProductP.UnitsOnOrder),
                            PK.FromEnum(ProductP.ReorderLevel),
                            PK.FromEnum(ProductP.Discontinued)
                        }
                    ),
                    (
                        "INSERT \"Shippers\"(\"ShipperID\",\"CompanyName\",\"Phone\") VALUES(",
                        nameof(Shipper),
                        new List<PK> {
                            PK.FromEnum(ShipperP.CompanyName),
                            PK.FromEnum(ShipperP.Phone)
                        }
                    ),
                    (
                        "INSERT \"Suppliers\"(\"SupplierID\",\"CompanyName\",\"ContactName\",\"ContactTitle\",\"Address\",\"City\",\"Region\",\"PostalCode\",\"Country\",\"Phone\",\"Fax\",\"HomePage\") VALUES(",
                        nameof(Supplier),
                        new List<PK> {
                            PK.FromEnum (SupplierP.CompanyName),
                            PK.FromEnum (SupplierP.ContactName),
                            PK.FromEnum (SupplierP.ContactTitle),
                            PK.FromEnum (SupplierP.Address),
                            PK.FromEnum (SupplierP.City),
                            PK.FromEnum (SupplierP.Region),
                            PK.FromEnum (SupplierP.PostalCode),
                            PK.FromEnum (SupplierP.Country),
                            PK.FromEnum (SupplierP.Phone),
                            PK.FromEnum (SupplierP.Fax),
                            PK.FromEnum (SupplierP.HomePage)
                        }
                    ),
                    (
                        "Insert Into Region Values (",
                        nameof(Region),
                        new List<PK> {
                            PK.FromEnum(RegionP.Description)
                        }
                    ),
                    (
                        "Insert Into Territories Values (",
                        nameof(Territory),
                        new List<PK> {
                            PK.FromEnum(TerritoryP.Description),
                            PK.FromEnum(TerritoryP.RegionId),
                        }
                    ),
                    (
                        "Insert Into EmployeeTerritories Values (",
                        nameof(EmployeeTerritory),
                        new List<PK> {
                            PK.FromEnum(EmployeeTerritoryP.EmployeeId),
                            PK.FromEnum(EmployeeTerritoryP.TerritoryId),
                        }
                    ),
                    (
                        "INSERT INTO \"Orders\" (\"OrderID\",\"CustomerID\",\"EmployeeID\",\"OrderDate\",\"RequiredDate\",\"ShippedDate\",\"ShipVia\",\"Freight\",\"ShipName\",\"ShipAddress\",\"ShipCity\",\"ShipRegion\",\"ShipPostalCode\",\"ShipCountry\") VALUES (",
                        nameof(Order),
                        new List<PK> {
                            PK.FromEnum(OrderP.CustomerId),
                            PK.FromEnum(OrderP.EmployeeId),
                            PK.FromEnum(OrderP.OrderDate),
                            PK.FromEnum(OrderP.RequiredDate),
                            PK.FromEnum(OrderP.ShippedDate),
                            PK.FromEnum(OrderP.ShipperId), // ShipperId?
                            PK.FromEnum(OrderP.Freight),
                            PK.FromEnum(OrderP.ShipName),
                            PK.FromEnum(OrderP.ShipAddress),
                            PK.FromEnum(OrderP.ShipCity),
                            PK.FromEnum(OrderP.ShipRegion),
                            PK.FromEnum(OrderP.ShipPostalCode),
                            PK.FromEnum(OrderP.ShipCountry)
                        }
                    )
                };

                var splitter = new Func<string, List<string>>(s => {
                    // Split on commas, remove apostrophes
                    var retval = new List<string>();
                    var pos = 0;
                    while (true) {
                        if (s[pos] == 'N' && pos < s.Length - 1 && s[pos + 1] == '\'') {
                            // String constant starting with N'
                            pos++;
                        }
                        if (s[pos] == '\'') {
                            // This is a string-value
                            // Find next occurence
                            var next = s.IndexOf('\'', pos + 1);
                            if (next == -1) throw new NorthwindConversionException("Trailing ' not found from position " + (pos + 1) + " in " + s);
                            while (next < s.Length - 1 && s[next + 1] == '\'') {
                                // Quote is part of field, like "'Bon app'''"
                                next = s.IndexOf('\'', next + 2);
                                if (next == -1) throw new NorthwindConversionException("Trailing ' not found from position " + (pos + 1) + " in " + s);
                            }
                            retval.Add(s.Substring(pos + 1, next - pos - 1).Replace("''", "'"));
                            pos = next + 1;
                            if (pos == s.Length) return retval; // We are finished
                        } else {
                            // This is not a string-value
                            var next = s.IndexOf(',', pos + 1);
                            if (next == -1) {
                                retval.Add(s[pos..]);
                                return retval; // We are finished
                            }
                            retval.Add(s[pos..next]);
                            pos = next;
                        }
                        if (s[pos] != ',') throw new NorthwindConversionException("Comma not found in position " + pos + " in " + s);
                        pos++;
                        if (retval.Count > 100) throw new NorthwindConversionException("Too many fields (" + retval.Count + ") in " + s);
                    }
                });

                static byte[] ToHexByte(string inputHex) {
                    // Borrowed from
                    // https://stackoverflow.com/questions/46327156/convert-a-hex-string-to-base64
                    var resultantArray = new byte[inputHex.Length / 2];
                    for (var i = 0; i < resultantArray.Length; i++) {
                        resultantArray[i] = System.Convert.ToByte(inputHex.Substring(i * 2, 2), 16);
                    }
                    return resultantArray;
                }

                var usCulture = new System.Globalization.CultureInfo("en-US", useUserOverride: false);
                var validDateTimeFormats = new string[] {
                    "M/d/yyyy",
                    //"M/dd/yyyy",
                    //"MM/d/yyyy",
                    //"MM/dd/yyyy"
                };
                source.Where(s => s.StartsWith("INSERT") || s.StartsWith("Insert")).ForEach(s => {
                    foreach (var (start, entityType, fields) in filters) {
                        if (s.StartsWith(start)) {
                            s = s[start.Length..];
                            if (s.EndsWith(")")) {
                                s = s[0..^1];
                            } else if (s.EndsWith(")\t")) {
                                s = s[0..^2];
                            } else {
                                throw new NorthwindConversionException("!s.EndsWith(\")\") for " + s);
                            }
                            var t = splitter(s);

                            // Insert primary keys for many-to-many relations
                            if (entityType == nameof(OrderDetail)) {
                                // Key is OrderId + ProductId
                                t.Insert(0, t[0] + "_" + t[1]);
                            } else if (entityType == nameof(EmployeeTerritory)) {
                                // Key is EmployeeId + TerritoryId
                                t.Insert(0, t[0] + "_" + t[1]);
                            }

                            // First item in t is the id-field (primary key)
                            if ((t.Count - 1) != fields.Count) throw new NorthwindConversionException("(t.Count - 1) (" + (t.Count - 1) + ") != fields.Count (" + fields.Count + ") for\r\n" + s);

                            for (var i = 0; i < fields.Count; i++) {
                                var value = t[i + 1]; // +1 because key is in t[0], and that does not count as a field.
                                if ("NULL".Equals(value)) {
                                    // No need in AgoRapide to specify null
                                    continue;
                                }

                                var strFieldName = fields[i].ToString();
                                if (strFieldName.EndsWith("Date")) {
                                    if (!DateTime.TryParseExact(value, validDateTimeFormats, usCulture, System.Globalization.DateTimeStyles.None, out var dtmValue)) {
                                        throw new NorthwindConversionException("DateTime.TryParseExact for field " + fields[i] + " (" + value + ") in\r\n" + s);
                                    }
                                    value = dtmValue.ToString("yyyy-MM-dd");
                                } else {
                                    switch (strFieldName) {
                                        case nameof(EmployeeP.Photo):
                                        case nameof(CategoryP.Picture):
                                            /// Convert to base 64 now. Remove first initial '0x' and then 78 byte OLE header.
                                            /// This saves space in property stream (From 1.5 to 1.3 MB) and makes for less work in <see cref="PictureEncoder.Encode"/>
                                            value = System.Convert.ToBase64String(ToHexByte(value[(2 + 78 * 2)..]));
                                            break;
                                    }
                                }

                                if (!fields[i].TryValidateAndParse(value, out var result)) throw new NorthwindConversionException("!fields[i].TryValidateAndParse (" + result.ErrorResponse + ") for field " + fields[i] + " in\r\n" + s);
                                ps.Add(nameof(PSPrefix.dt) + "/" +
                                    entityType + "/" +
                                    t[0] + "/" + // Entity primary key
                                    strFieldName + " = " +

                                    // NOTE: You can also use a one-liner here:
                                    // NOTE: value.Replace("0x", "0xoooo").Replace("\r", "0x000D").Replace("\n", "0x000A").Replace(";", "0x003B");
                                    PropertyStreamLine.EncodeValuePart(value)); // Value
                            }
                            break;
                        }
                    }
                });

                logger.LogKeyValue(nameof(ps) + ".Count", ps.Count.ToString());

                // Leading _ should ensure that file will be read first at next startup
                var psPath = Environment.CurrentDirectory + System.IO.Path.DirectorySeparatorChar + "Data" + System.IO.Path.DirectorySeparatorChar + "_instnwnd.txt";
                logger.LogKeyValue(nameof(psPath), psPath);
                System.IO.File.WriteAllText(psPath, string.Join("\r\n", ps) + "\r\n"); // Extremely important, remember trailing line break

                throw new NorthwindConversionSuccessfulException(
                    "File " + sourcePath + " has been successfully converted to " + nameof(ARConcepts.PropertyStream) + "-format as " + psPath + ".\r\n" +
                    "Resolution:\r\nRestart application in order to continue."
                );
            }
        }

        private class NorthwindConversionException : ApplicationException {
            public NorthwindConversionException(string message) : base(message) { }
            public NorthwindConversionException(string message, Exception innerException) : base(message, innerException) { }
        }

        private class NorthwindConversionSuccessfulException : ApplicationException {
            public NorthwindConversionSuccessfulException(string message) : base(message) { }
            public NorthwindConversionSuccessfulException(string message, Exception innerException) : base(message, innerException) { }
        }
    }
}
