REM Ensure that Build | Publish ARNorthwind was done in Visual Studio.
REM
REM Ensure that station Z: is mapped to root of the web server
REM
REM In other words, ensure that the following folders are available:
REM Z:\etc\apache2\sites-enabled (for copying of file ARNorthwind.conf)
REM Z:\etc\systemd\system (for copying of file ARNorthwind.service)
REM Z:\var\www\app\ARNorthwind (for copying of ARNorthwind\bin\Release\net5.0\publish)

PAUSE

COPY ARNorthwind.conf Z:\etc\apache2\sites-enabled
COPY ARNorthwind.service Z:\etc\systemd\system
COPY ..\..\bin\Release\net5.0\publish\*.* Z:\var\www\app\ARNorthwind

REM Most relevant commands to execute on server now are:
REM   systemctl restart ARNorthwind.service

REM If ARNorthwind.conf was changed:
REM   apache2ctl configtest
REM   systemctl restart apache2

REM If ARNorthwind.service was changed:
REM   systemctl daemon-reload
REM   systemctl restart ARNorthwind.service

REM If this was initial publish, additional steps must also be taken.
REM See ARNorthwind.conf and ARNorthwind.service for details.
REM Also, ensure that folder \var\www\app\ARNorthwind\Data AND files within have necessary write rights.

PAUSE


