﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description = "Represents a Region. See also -" + nameof(RegionP) + "-.")]
    public class Region : PRich {
    }

    [Enum(
        Description = "Describes class -" + nameof(Region) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum RegionP {
        __invalid,
        Description
    }
}
