﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description = "Represents an Employee's territory. See also -" + nameof(EmployeeTerritoryP) + "-.")]
    public class EmployeeTerritory : PRich {
    }

    [Enum(
        Description = "Describes class -" + nameof(EmployeeTerritory) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum EmployeeTerritoryP {
        __invalid,
        EmployeeId,
        TerritoryId
    }
}
