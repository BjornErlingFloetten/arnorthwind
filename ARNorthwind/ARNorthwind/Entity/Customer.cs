﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description = "Represents a Customer. See also -" + nameof(CustomerP) + "-.")]
    public class Customer : PRich {
    }

    [Enum(
        Description = "Describes class -" + nameof(Customer) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum CustomerP {
        __invalid,
        CompanyName,
        ContactName,
        ContactTitle,
        Address,
        City,
        [PKType(Description = "NOTE: No relation to -" + nameof(Region) + "-, therefore not called RegionId")]
        Region,
        PostalCode,
        Country,
        Phone,
        Fax
    }
}
