﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using System.Text;

namespace ARNorthwind
{

    [Class(Description = "Represents an Employee. See also -" + nameof(EmployeeP) + "-.")]
    public class Employee : PRich
    {

        [ClassMember(Description =
            "Adds together LastName, FirstName, TitleOfCourtesy, Title\r\n" +
            "Returns TRUE if finds one of those.\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetName(out IP retval, out string errorResponse)
        {
            var sb = new StringBuilder();
            new List<EmployeeP> { EmployeeP.LastName, EmployeeP.FirstName, EmployeeP.TitleOfCourtesy, EmployeeP.Title }.ForEach(p =>
            {
                if (IP.TryGetPV<string>(p, out var v))
                {
                    if (sb.Length > 0) sb.Append(", ");
                    sb.Append(v);
                }
            });
            if (sb.Length == 0)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "None of LastName, FirstName, TitleOfCourtesy, Title found.";
                return false;
            }
            retval = new PValue<string>(sb.ToString());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    [Enum(
        Description = "Describes class -" + nameof(Employee) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum EmployeeP
    {
        __invalid,
        LastName,
        FirstName,
        Title,
        TitleOfCourtesy,
        [PKType(Type = typeof(DateTime))]
        BirthDate,
        [PKType(Type = typeof(DateTime))]
        HireDate,
        Address,
        City,
        [PKType(Description = "NOTE: No relation to -" + nameof(Region) + "-, therefore not called RegionId")]
        Region,
        PostalCode,
        Country,
        HomePhone,
        Extension,
        [PKHTML(Encoder = typeof(PictureEncoder))]
        Photo,
        Notes,

        [PKType(Description ="NOTE: Called 'ReportsTo' in original database.")]
        [PKRel(ForeignEntity = typeof(Employee), OppositeTerm = "Subordinate")]
        SupervisorId,
        
        PhotoPath
    }
}
