﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description = "Represents a Supplier. See also -" + nameof(SupplierP) + "-.")]
    public class Supplier : PRich {
    }

    [Enum(
        Description = "Describes class -" + nameof(Supplier) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum SupplierP {
        __invalid,
        CompanyName,
        ContactName,
        ContactTitle,
        Address,
        City,
        [PKType(Description = "NOTE: No relation to -" + nameof(Region) + "-, therefore not called RegionId")]
        Region,
        PostalCode,
        Country,
        Phone,
        Fax, 
        HomePage
    }
}
