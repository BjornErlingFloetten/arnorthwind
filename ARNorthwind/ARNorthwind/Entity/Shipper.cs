﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description = "Represents a Shipper. See also -" + nameof(ShipperP) + "-.")]
    public class Shipper : PRich {
    }

    [Enum(
        Description = "Describes class -" + nameof(Shipper) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum ShipperP {
        __invalid,
        CompanyName,
        Phone,
    }
}
