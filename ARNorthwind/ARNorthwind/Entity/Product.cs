﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description = "Represents a Product. See also -" + nameof(ProductP) + "-.")]
    public class Product : PRich {
    }

    [Enum(
        Description = "Describes class -" + nameof(Product) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum ProductP {
        __invalid,
        Name,
        SupplierId,
        CategoryId,
        QuantityPerUnit,
        UnitPrice,
        UnitsInStock,
        UnitsOnOrder,
        ReorderLevel,
        Discontinued
    }
}
