﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description =
        "Represents an Order detail (Order line). See also -" + nameof(OrderDetailP) + "- and -" + nameof(OrderDetailCollection) + "-.\r\n" +
        "\r\n" +
        "NOTE: As given this class inherits -" + nameof(PRich) + ".\r\n" +
        "NOTE: A more memory efficient alternative,\r\n" +
        "NOTE: since there exists many order details and a typical order detail object is densely populated (contains most of its possible values),\r\n" +
        "NOTE: would be to inherit -" + nameof(PExact<TPropertyKeyEnum>) + "- like this:\r\n" +
        "NOTE:   public class OrderDetail : PExact<OrderDetailP> {\r\n" +
        "NOTE: This will at the same time restrict instances of this class to only store keys from -" + nameof(OrderDetailP) + "-\r\n" +
        "NOTE: (in constrast to -" + nameof(PRich) + "- which can store any property without restrictions)."
    )]
    public class OrderDetail : PRich {

        [ClassMember(Description =
            "Calculates sum based on Quantity, UnitPrice and Discount.\r\n" +
            "Returns TRUE if finds the first two.\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetSum(out IP retval, out string errorResponse) {
            if (!IP.TryGetPV<double>(OrderDetailP.UnitPrice, out var price) || !IP.TryGetPV<double>(OrderDetailP.Quantity, out var qty)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "UnitPrice and / or Quantity not found.";
                return false;
            }
            retval = new PValue<double>(price * qty * (1 - IP.GetPV<double>(OrderDetailP.Discount)));
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key " +
        "-" + nameof(OrderDetailP.OrderId) + "- and " +
        "-" + nameof(OrderDetailP.ProductId) + "-." +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(Order) + "- instances.\r\n"
    )]
    public class OrderDetailCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(OrderDetail) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum OrderDetailP {
        __invalid,
        OrderId,
        ProductId,
        [PKType(Type=typeof(double))]
        UnitPrice,
        [PKType(Type = typeof(int))]
        Quantity,
        [PKType(Type = typeof(double))]
        Discount,
    }
}