﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description = "Represents a Territory. See also -" + nameof(TerritoryP) + "-.")]
    public class Territory : PRich {
    }

    [Enum(
        Description = "Describes class -" + nameof(Territory) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum TerritoryP {
        __invalid,
        Description,
        RegionId
    }
}
