﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARNorthwind {

    [Class(Description = "Represents a Category (of products). See also -" + nameof(CategoryP) + "-.")]
    public class Category : PRich {
    }

    [Enum(
        Description = "Describes class -" + nameof(Category) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum CategoryP {
        __invalid,
        Name,
        Description,
        [PKHTML(Encoder = typeof(PictureEncoder))]
        Picture
    }
}
