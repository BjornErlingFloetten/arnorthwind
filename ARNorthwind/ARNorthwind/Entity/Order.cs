﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARNorthwind {

    [Class(Description =
        "Represents an Order. See also -" + nameof(OrderP) + "- and -" + nameof(OrderCollection) + "-.\r\n" +
        "\r\n" +
        "NOTE: As given this class inherits -" + nameof(PRich) + ".\r\n" +
        "NOTE: A more memory efficient alternative, " +
        "NOTE: since there exists many orders and a typical order object is densely populated (contains most of its possible values),\r\n" +
        "NOTE: would be to inherit -" + nameof(PExact<TPropertyKeyEnum>) + "- like this:\r\n" +
        "NOTE:   public class Order : PExact<OrderP> {\r\n" +
        "NOTE: This will at the same time restrict instances of this class to only store keys from -" + nameof(OrderP) + "-\r\n" +
        "NOTE: (in constrast to -" + nameof(PRich) + "- which can store any property without restrictions)."
    )]
    public class Order : PRich {
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign keys " +
        "-" + nameof(OrderP.CustomerId) +"-, " +
        "-" + nameof(OrderP.EmployeeId) + "- and " +
        "-" + nameof(OrderP.ShipperId) + "-. " +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(Order) + "- instances.\r\n"
    )]
    public class OrderCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(Order) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum OrderP {
        __invalid,
        CustomerId,
        EmployeeId,
        [PKType(Type = typeof(DateTime))]
        OrderDate,
        [PKType(Type = typeof(DateTime))]
        RequiredDate,
        [PKType(Type = typeof(DateTime))]
        ShippedDate,
        [PKType(Description = "Called 'ShipVia' in original sample database")]
        ShipperId,
        Freight,
        ShipName,
        ShipAddress,
        ShipCity,
        ShipRegion,
        ShipPostalCode,
        ShipCountry
    }
}
